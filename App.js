import { StatusBar } from 'expo-status-bar';
import {SafeAreaView, ScrollView, StyleSheet, Text, View, FlatList } from 'react-native';
import { useState } from 'react';

export default function App() {
    const [capsules, setCapsules] = useState([])

    const getCapsules = () => {
        fetch('https://api.spacexdata.com/v4/capsules')
        .then((res) => {
          return res.json()
        })
        .then((data)=>{
          console.log(data[0])
          setCapsules(data)
        })
    }  

    getCapsules()
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.container}>
            {
              capsules.map((item)=>{
                return(
                  <View style={styles.item}>
                    <Text style={styles.title}>{item.serial} {item.type}</Text>
                  </View>)
              })
            }
          </View>
        </ScrollView>
      </SafeAreaView>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EfFF',
    paddingTop: StatusBar.currentHeight,
  },
  scrollView: {
    backgroundColor: 'pink',
    marginHorizontal: 5,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginTop: 10,
  },
  title: {
    fontSize: 15,
  },
});
